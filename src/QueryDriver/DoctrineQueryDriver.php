<?php
namespace Agnostic\QueryDriver;

use Agnostic\QueryDriver\QueryDriverInterface;
use Doctrine\DBAL\Connection;

class DoctrineQueryDriver implements QueryDriverInterface
{
    protected $conn;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    /**
     * {@inheritdoc}
     *
     * @return Doctrine\DBAL\Query\QueryBuilder
     */
    public function createQuery($table_name = null)
    {
        $query_builder = $this->conn->createQueryBuilder();

        if ($table_name) {
            $root_alias = substr($table_name, 0, 1);
            $query_builder
                ->select(sprintf('%s.*', $root_alias))
                ->from($table_name, $root_alias)
                ;
        }

        return $query_builder;
    }

    public function addWhereIn($query_builder, $field, array $values)
    {
        $query_builder->andWhere($query_builder->expr()->in($field, $values));

        return $query_builder;
    }
    public function addWhere($query_builder, $query, $value=false)
    {
        $rs = preg_match('/(:[\w]+)/i', $query, $match);
        $query_builder->andWhere($query);
        if ($rs) {
            $param = $match[1];
            $query_builder->setParameter($param, $value);
        }

        return $query_builder;
    }
    public function orderBy($query_builder, $order) {
        preg_match('/(\w+)\s(DESC|ASC)?/i', $order, $match);
        $sort = $match[1];
        $order = $match[2]?:'ASC';
        return $query_builder->addOrderBy($sort, $order);
    }
    public function limit($query_builder, $offset=0, $limit=0) {
        if ($limit) {
            $query_builder->setMaxResults($limit);
        }
        if ($offset) {
            $query_builder->setFirstResult($offset);
        }
        return $query_builder;
    }

    public function fetchData($query_builder, array $opts = [])
    {
        $stmt = $query_builder->execute();
        $data = $stmt->fetchAll();
        return $data;
    }

    public function toSql($query_builder)
    {
        return (string)$query_builder;
    }
}
