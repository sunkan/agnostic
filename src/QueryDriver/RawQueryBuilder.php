<?php
namespace Agnostic\QueryDriver;

/**
 * Raw plain string query.
 */
class RawQueryBuilder
{
    protected $table;
    protected $where = [];
    protected $order = [];
    protected $limit = 0;
    protected $offset = 0;
    protected $_build;

    public function __construct($table) {
        $this->table = $table;
    }

    public function addWhere($query, $value=false) {
        if ($value===false) {
            $this->where[] = $query;
        } else {
            $this->where[] = [$query, $value];
        }
        return $this;
    }
    public function addWhereIn($field, array $values) {
        $sql = '%s IN (%s)';
        if (is_int($values[0])) {
            $this->addWhere(sprintf($sql, $field, implode(',', $values)));
        } else {
            $newValues = [];
            foreach ($values as $value) {
                $newValues[] = "'".$value."'";
            }
            $this->addWhere(sprintf($sql, $field, implode(',', $newValues)));
        }
        return $this;
    }

    public function orderBy($order) {
        $this->order[] = $order;
        return $this;
    }
    public function limit($offset, $limit) {
        $this->offset = (int)$offset;
        $this->limit = (int)$limit;
        return $this;
    }
    public function build() {
        if ($this->_build) {
            return $this->_build;
        }
        list($where, $params) = $this->_buildWhere($this->where);
        
        $sql = 'SELECT * FROM %s WHERE %s';
        $sql = sprintf($sql, $this->table, $where);
        if (count($this->order)) {
            $sql .= ' ORDER BY '.implode(',', $this->order);
        }
        if ($this->limit) {
            $sql .= sprintf(' LIMIT %d, %d', $this->offset, $this->limit);
        }
        return $this->_build = ['sql'=>$sql, 'params'=>$params];
    }

    public function toSql() {
        $data = $this->build();
        return $data['sql'];
    }
    public function getParams() {
        $data = $this->build();
        return $data['params'];
    }


    protected function _buildWhere($where) {
        if (count($where)==0) {
            return array(' 1=1 ', array());
        }
        $sql = '';
        $params = array();
        foreach($where as $k) {
            if (is_array($k)) {
                list($q, $v) = $k;
                $sql .= ' '.$q .' AND';
                if (is_array($v)) {
                    foreach ($v as $value) {
                        $params[] = $value;
                    }
                } else {
                    $params[] = $v;
                }

            } else {
                $sql .= ' '.$k.' AND';
            }
        }
        if (substr($sql, -4)==' AND') {
            $sql = substr($sql, 0, -4);
        }
        return array($sql, $params);
    }

    public function __toString()
    {
        return $this->build();
    }
}
