<?php
namespace Agnostic\QueryDriver;

use Agnostic\QueryDriver\QueryDriverInterface;
use PDO;
use Agnostic\QueryDriver\RawQueryBuilder;

class RawQueryDriver implements QueryDriverInterface
{
    protected $conn;

    /**
     * @param PDO
     */
    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @inheritdoc
     */
    public function createQuery($table_name = null)
    {
        $query_builder = new RawQueryBuilder($table_name);
        return $query_builder;
    }

    /**
     * @param RawQueryBuilder
     * @param string
     * @param array
     * @return RawQueryBuilder
     */
    public function addWhereIn($query_builder, $field, array $values)
    {
        if (!empty($values)) {
            $query_builder->addWhereIn($field, $values);
        }

        return $query_builder;
    }

    public function addWhere($query_builder, $query, $value=false) {
        return $query_builder->addWhere($query, $value);
    }

    public function orderBy($query_builder, $order) {
        return $query_builder->orderBy($order);
    }
    public function limit($query_builder, $offset = 0, $limit = 0) {
        return $query_builder->limit($offset, $limit);
    }

    /**
     * @param RawQueryBuilder
     * @param array
     * @return mixed
     */
    public function fetchData($query_builder, array $opts = [])
    {
        $fetch_style = isset($opts['fetch_style']) ? $opts['fetch_style'] : PDO::FETCH_ASSOC;
        $stmt = $this->conn->prepare($query_builder->toSql());
        $stmt->execute($query_builder->getParams());
        $data = $stmt->fetchAll($fetch_style);

        return $data;
    }

    /**
     * @param RawQueryBuilder
     * @return string
     */
    public function toSql($query_builder)
    {
        return $query_builder->toSql().' ['.implode(',', $query_builder->getParams()).']';
    }
}
